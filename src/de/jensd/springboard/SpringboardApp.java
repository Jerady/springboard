/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.springboard;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters
 */
public class SpringboardApp extends Application
{

    @Override
    public void start(Stage primaryStage)
    {

        ObservableList<Starter> springs = FXCollections
                .observableArrayList();

        String hoverBaseImage = "/de/jensd/springboard/cloud_download.png";


        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));
        springs
                .add(new Starter(hoverBaseImage, 100, 100));

        AnchorPane springPane = new AnchorPane();
        springPane.getChildren()
                .addAll(springs);


        double angle = 0;
        double angleStep = 360 / springs.size();
        double centerX = 200.0;
        double centerY = 200.0;

        for (Starter hoverSpringBoard : springs)
        {
            hoverSpringBoard.setLayoutX(150.0 * Math.cos(Math
                    .toRadians(angle)) + centerX);
            hoverSpringBoard.setLayoutY(150.0 * Math.sin(Math
                    .toRadians(angle)) + centerY);

            angle += angleStep;
        }

        StackPane root = new StackPane();
        root.getChildren()
                .addAll(springPane);

        Scene scene = new Scene(root, 500, 500);

        primaryStage.setTitle("SpringBoardDemo");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
