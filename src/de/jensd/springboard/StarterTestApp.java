/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.springboard;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Jens Deters
 */
public class StarterTestApp extends Application
{

    @Override
    public void start(Stage primaryStage)
    {

        ObservableList<Starter> springs = FXCollections
                .observableArrayList();

        Starter starter1 = new Starter("/de/jensd/springboard/cloud_download.png", 200, 200);
        starter1.setLayoutX(150.0);
        starter1.setLayoutY(150.0);
        AnchorPane springPane = new AnchorPane();
        springPane.getChildren()
                .addAll(starter1);

        StackPane root = new StackPane();
        root.getChildren()
                .addAll(springPane);

        Scene scene = new Scene(root, 500, 500);

        primaryStage.setTitle("SpringBoardDemo");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args)
    {
        launch(args);
    }
}
